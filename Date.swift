//
//  Date.swift
//  Whether
//
//  Created by Chris Bernardi on 1/28/15.
//  Copyright (c) 2015 Chris Bernardi. All rights reserved.
//

import Foundation
import UIKit


let date = NSDate()
let calendar = NSCalendar.currentCalendar()
let components = calendar.components(.CalendarUnitHour | .CalendarUnitMinute | .CalendarUnitMonth | .CalendarUnitYear | .CalendarUnitDay, fromDate: date)
var hour = components.hour
let minutes = components.minute
let month = components.month
let year = components.year
let day = components.day

func timeOfDay(hour: Int) -> (String) {
    var timeOfDay: String
    
    if hour >= 0 && hour <= 11 {
        timeOfDay = "\(hour)A"
    } else if hour == 12 {
        timeOfDay = "\(hour)P"
    } else if hour >= 13 && hour <= 24 {
        timeOfDay = "\(hour - 12)P"
    } else if hour >= 25 {
        timeOfDay = "\(hour - 24)A"
    } else {
        timeOfDay = "\(hour)"
    }
    
    return timeOfDay
}


let bgBlue = UIColor(red: 52/255.0, green: 152/255.0, blue: 219/255.0, alpha: 1.0)
let bgYellow = UIColor(red: 243/255.0, green: 156/255.0, blue: 18/255.0, alpha: 1.0)
let bgGray = UIColor(red: 44/255.0, green: 62/255.0, blue: 80/255.0, alpha: 1.0)


func timeBasedBackgroundColor(hour: Int) -> UIColor {
    var backgroundColor: UIColor
    
    if hour >= 0 && hour <= 5 {
        backgroundColor = bgGray
    } else if hour >= 5 && hour <= 10 {
        backgroundColor = bgBlue
    } else if hour >= 10 && hour <= 16 {
        backgroundColor = bgYellow
    } else if hour >= 16 && hour <= 24 {
        backgroundColor = bgGray
    } else {
        backgroundColor = bgBlue
    }
    
    return backgroundColor
}

func timeBasedGreeting(hour: Int) -> String {
    var greetingText: String
    
    if hour >= 0 && hour <= 5 {
        greetingText = "Sleep now. Sleep. "
    } else if hour >= 5 && hour <= 10 {
        greetingText = "Good Morning, "
    } else if hour >= 10 && hour <= 16 {
        greetingText = "Good Afternoon, "
    } else if hour >= 16 && hour <= 24 {
        greetingText = "Good Evening, "
    } else {
        greetingText = "Time traveler?"
    }
    
    return greetingText
}


// TODO - check to see if this is still needed
func hourLabels(hour: Int) -> String {
    var hourLabelText: String
    
    if hour >= 1 && hour <= 11 {
        hourLabelText = "\(hour)AM"
    } else if hour == 12 {
        hourLabelText = "\(hour)PM"
    } else if hour >= 13 && hour <= 23 {
        hourLabelText = "\(hour)PM"
    } else if hour == 24 {
        hourLabelText = "\(hour)PM"
    } else {
        hourLabelText = "\(hour)"
    }
    
    return hourLabelText
}


// TODO - check to see if this is still needed
func timeBasedTextColor(hour: Int) -> UIColor {
    var textColor: UIColor
    
    if hour > 5 && hour < 10 {
        textColor = UIColor.whiteColor()
    } else if hour > 10 && hour < 16 {
        textColor = UIColor.orangeColor()
    } else {
        textColor = UIColor.whiteColor()
    }
    
    return textColor
}


