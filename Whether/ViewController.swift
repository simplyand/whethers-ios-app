//
//  ViewController.swift
//  Whether
//
//  Created by Chris Bernardi on 1/7/15.
//  Copyright (c) 2015 Chris Bernardi. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
//import SwiftyJSON
//import WhereAmI

class ViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var warningMessageView: UIView!
    @IBOutlet weak var currentTemperatureLabel: UILabel!
    @IBOutlet weak var icon: UILabel!
    @IBOutlet weak var currentLocationLabel: UILabel!
    @IBOutlet weak var greetingLabel: UILabel!
    @IBOutlet weak var hiTemperatureLabel: UILabel!
    @IBOutlet weak var lowTemperatureLabel: UILabel!
    @IBOutlet weak var weatherWarningIcon: UILabel!
    @IBOutlet weak var weatherWarningLabel: UILabel!
    @IBOutlet weak var hourlySummaryLabel: UILabel!
//    @IBOutlet weak var hourlyTempOne: UILabel!
//    @IBOutlet weak var hourlyTempOneTime: UILabel!
//    @IBOutlet weak var hourlyTempOneIcon: UILabel!
    @IBOutlet weak var hourlyTempTwo: UILabel!
    @IBOutlet weak var hourlyTempTwoTime: UILabel!
    @IBOutlet weak var hourlyTempTwoIcon: UILabel!
    @IBOutlet weak var hourlyTempThree: UILabel!
    @IBOutlet weak var hourlyTempThreeTime: UILabel!
    @IBOutlet weak var hourlyTempThreeIcon: UILabel!
    @IBOutlet weak var hourlyTempFour: UILabel!
    @IBOutlet weak var hourlyTempFourTime: UILabel!
    @IBOutlet weak var hourlyTempFourIcon: UILabel!
    @IBOutlet weak var tomorrowIcon: UILabel!
    @IBOutlet weak var tomorrowLowTemp: UILabel!
    @IBOutlet weak var tomorrowHIghTemp: UILabel!
    @IBOutlet weak var dayAfterTomorrowIcon: UILabel!
    @IBOutlet weak var dayAfterLowTemp: UILabel!
    @IBOutlet weak var dayAfterHighTemp: UILabel!
    @IBOutlet weak var dayAfterThatIcon: UILabel!
    @IBOutlet weak var dayAfterThatLowTemp: UILabel!
    @IBOutlet weak var dayAfterThatHighTemp: UILabel!
    
    
    private let apiKey = "4e86937d09a58373690c3adff37553de"
    
    //let locationManager = CLLocationManager()
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        locationManager.delegate = self
//        locationManager.requestWhenInUseAuthorization()
//        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//        locationManager.startUpdatingLocation()
        
        let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate! as! AppDelegate
        appDelegate.myViewController = self
        println("View Loaded")
        
        self.loadingView.hidden = false
        
    }
    
    
    
    
    // Reverse Geocode
//    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
//        println("Step 1 - Reverse Geocode")
//        
//        
//        CLGeocoder().reverseGeocodeLocation(manager.location, completionHandler: {(placemarks, error)->Void in
//            
//            if (error != nil) {
//                println("Reverse geocoder failed with error" + error.localizedDescription)
//                return
//            }
//            
//            if placemarks.count > 0 {
//                let pm = placemarks[0] as! CLPlacemark
//                self.displayLocationInfo(pm)
//            } else {
//                println("Problem with the data received from geocoder")
//            }
//        })
//    }
    
    // Displays Reverse Geocoded Location Info
//    func displayLocationInfo(placemark: CLPlacemark?) {
//        println("Step 2 - Displays Reverse Geocoded Location Info")
//        
//        if let containsPlacemark = placemark {
//            locationManager.stopUpdatingLocation()
//            let locality = (containsPlacemark.locality != nil) ? containsPlacemark.locality : ""
//            
//            // Updates Location Label
//            currentLocationLabel.text = " \(locality)."
//        }
//        
//    }
    
//    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
//        println("Error while updating location " + error.localizedDescription)
//        
//        let networkIssueController = UIAlertController(title: "Error", message: "Unable to load data. Connectivity error!", preferredStyle: .Alert)
//        
//        let okButton = UIAlertAction(title: "OK", style: .Default, handler: nil)
//        networkIssueController.addAction(okButton)
//        
//        let cancelButton = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
//        networkIssueController.addAction(cancelButton)
//        
//        
//        self.presentViewController(networkIssueController, animated: true, completion: nil)
//    }
    
    
    
    func getCurrentLocation() {
        
    }
    
    
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedAlways || status == .AuthorizedWhenInUse {
            getCurrentWeatherData()
        }
    }
    
    
    // For when view is loaded - debugging only
    func refreshEverything() {
        println("Everything is new")
        getCurrentWeatherData()
        locationManager.startUpdatingLocation()
        self.view.backgroundColor = timeBasedBackgroundColor(hour)

        
    }
    
    
    // Get Current Weather Info
    func getCurrentWeatherData() -> Void {
        println("Step 3 - Get Weather Info")
        
        self.view.backgroundColor = timeBasedBackgroundColor(hour)
        println(hour)
        
        let location = locationManager.location
        var locValue:CLLocationCoordinate2D = location.coordinate
        let currentLat = locValue.latitude
        let currentLong = locValue.longitude
        
        let urlPath = "https://api.forecast.io/forecast/\(apiKey)/\(currentLat),\(currentLong)?exclude=minutely&units=auto"
        println(urlPath)
        let url: NSURL = NSURL(string: urlPath)!
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithURL(url, completionHandler: {data, response, error -> Void in
            
            
            
            var err: NSError?
            var jsonResult = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as! NSDictionary
            if err != nil {
                // If there is an error parsing JSON, print it to the console
                println("JSON Error \(err!.localizedDescription)")
            }
            
            if (error == nil) {
            
                let json = JSON(jsonResult)
                
                
                // gets the current conditions
                let currentTemp = json["currently"]["temperature"].intValue
                var currentIcon = json["currently"]["icon"].stringValue
                let todayLowTemp = json["daily"]["data"][0]["temperatureMin"].intValue
                let todayHighTemp = json["daily"]["data"][0]["temperatureMax"].intValue
                let weatherWarning = json["alerts"][0]["title"].stringValue
                let weatherWarningUrl = json["alerts"][0]["uri"].stringValue
                
                // gets the next 12 hours in 4 hour increments
                let hourlySummary = json["hourly"]["summary"].stringValue
//                let hourlyTempOne = json["hourly"]["data"][1]["temperature"].intValue
//                let hourlyOneIcon = json["hourly"]["data"][1]["icon"].stringValue
                let hourlyTempTwo = json["hourly"]["data"][3]["temperature"].intValue
                let hourlyTwoIcon = json["hourly"]["data"][3]["icon"].stringValue
                let hourlyTempThree = json["hourly"]["data"][6]["temperature"].intValue
                let hourlyThreeIcon = json["hourly"]["data"][6]["icon"].stringValue
                let hourlyTempFour = json["hourly"]["data"][9]["temperature"].intValue
                let hourlyFourIcon = json["hourly"]["data"][9]["icon"].stringValue
                
                
                // gets the next three days' conditions
                let tomorrowIcon = json["daily"]["data"][1]["icon"].stringValue
                let tomorrowLowTemp = json["daily"]["data"][1]["temperatureMin"].intValue
                let tomorrowHighTemp = json["daily"]["data"][1]["temperatureMax"].intValue
                let dayAfterTomorrowIcon = json["daily"]["data"][2]["icon"].stringValue
                let dayAfterTomorrowLowTemp = json["daily"]["data"][2]["temperatureMin"].intValue
                let dayAfterTomorrowHighTemp = json["daily"]["data"][2]["temperatureMax"].intValue
                let andTheDayAfterThatIcon = json["daily"]["data"][3]["icon"].stringValue
                let andTheDayAfterThatLowTemp = json["daily"]["data"][3]["temperatureMin"].intValue
                let andTheDayAfterThatHighTemp = json["daily"]["data"][3]["temperatureMax"].intValue
                
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.currentTemperatureLabel.text = "\(currentTemp)"
                    self.greetingLabel.text = timeBasedGreeting(hour)
                    self.icon.text = weatherIconFromString(currentIcon)
                    self.tomorrowIcon.text = weatherIconFromString(tomorrowIcon)
                    self.dayAfterTomorrowIcon.text = weatherIconFromString(dayAfterTomorrowIcon)
                    self.dayAfterThatIcon.text = weatherIconFromString(andTheDayAfterThatIcon)
                    self.lowTemperatureLabel.text = "ꜜ\(todayLowTemp)°"
                    self.hiTemperatureLabel.text = "ꜛ\(todayHighTemp)°"
                    if weatherWarning.isEmpty {
                        self.weatherWarningLabel.hidden = true
                        self.weatherWarningIcon.hidden = true
                        self.warningMessageView.hidden = true
                    } else {
                        self.warningMessageView.hidden = false
                        self.warningMessageView.backgroundColor = UIColor(red: 192/255, green: 57/255, blue: 43/255, alpha: 1.0)
                        self.weatherWarningIcon.hidden = false
                        self.weatherWarningLabel.text = "  " + "\(weatherWarning)"
                    }
                    self.hourlySummaryLabel.text = "\(hourlySummary)"
//                    self.hourlyTempOne.text = "\(hourlyTempOne)°"
//                    self.hourlyTempOneTime.text = "\(timeOfDay(hour + 1))"
//                    self.hourlyTempOneIcon.text = weatherIconFromString(hourlyOneIcon)
                    self.hourlyTempTwo.text = "\(hourlyTempTwo)°"
                    self.hourlyTempTwoTime.text = "\(timeOfDay(hour + 3))"
                    self.hourlyTempTwoIcon.text = weatherIconFromString(hourlyTwoIcon)
                    self.hourlyTempThree.text = "\(hourlyTempThree)°"
                    self.hourlyTempThreeTime.text = "\(timeOfDay(hour + 6))"
                    self.hourlyTempThreeIcon.text = weatherIconFromString(hourlyThreeIcon)
                    self.hourlyTempFour.text = "\(hourlyTempFour)°"
                    self.hourlyTempFourTime.text = "\(timeOfDay(hour + 9))"
                    self.hourlyTempFourIcon.text = weatherIconFromString(hourlyFourIcon)
                    self.tomorrowLowTemp.text = "ꜜ\(tomorrowLowTemp)°"
                    self.tomorrowHIghTemp.text = "ꜛ\(tomorrowHighTemp)°"
                    self.dayAfterLowTemp.text = "ꜜ\(dayAfterTomorrowLowTemp)°"
                    self.dayAfterHighTemp.text = "ꜛ\(dayAfterTomorrowHighTemp)°"
                    self.dayAfterThatLowTemp.text = "ꜜ\(andTheDayAfterThatLowTemp)°"
                    self.dayAfterThatHighTemp.text = "ꜛ\(andTheDayAfterThatHighTemp)°"
                    
                    self.loadingView.hidden = true
                })
            } else {
                let networkIssueController = UIAlertController(title: "Error", message: "Unable to load data. Connectivity error!", preferredStyle: .Alert)
                
                let okButton = UIAlertAction(title: "OK", style: .Default, handler: nil)
                networkIssueController.addAction(okButton)
                
                let cancelButton = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
                networkIssueController.addAction(cancelButton)
                
                
                self.presentViewController(networkIssueController, animated: true, completion: nil)
            }
        })
        task.resume()
        
    }
    
    @IBAction func weatherWarningWeb(sender: AnyObject) {
//        if let url = NSURL(string: "\(weatherWarningUrl)") {
//            UIApplication.sharedApplication().openURL(url)
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

