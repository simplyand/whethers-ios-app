//
//  IconNaming.swift
//  Whether
//
//  Created by Chris Bernardi on 1/27/15.
//  Copyright (c) 2015 Chris Bernardi. All rights reserved.
//

import Foundation

func weatherIconFromString(stringIcon: String) -> String {
    var iconName: String
    
    switch stringIcon {
    case "clear-day":
        iconName = "clear"
    case "clear-night":
        iconName = "moon"
    case "rain":
        iconName = "rain"
    case "snow":
        iconName = "snow"
    case "sleet":
        iconName = "sleet"
    case "wind":
        iconName = "wind"
    case "fog":
        iconName = "fog"
    case "cloudy":
        iconName = "cloudy"
    case "partly-cloudy-day":
        iconName = "partly cloudy"
    case "partly-cloudy-night":
        iconName = "cloudy night"
    case "hail":
        iconName = "heavy hail"
    case "thunderstorm":
        iconName = "thunderstorm"
    case "tornado":
        iconName = "tornado"
    default:
        iconName = ""
    }
    
    return iconName
}

